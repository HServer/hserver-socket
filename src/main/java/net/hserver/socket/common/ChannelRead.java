package net.hserver.socket.common;

import java.nio.channels.CompletionHandler;
import java.util.logging.Logger;

public class ChannelRead implements CompletionHandler<Integer, HSocketContext> {
    @Override
    public void completed(Integer result, HSocketContext hSocketContext) {
        if (result > 0) {
            //读数据
            hSocketContext.getHandler().onMessage(hSocketContext, hSocketContext.readData());
            //再次监听读
            hSocketContext.bindReadChannel();
        }
    }

    @Override
    public void failed(Throwable exc, HSocketContext hSocketContext) {
        hSocketContext.getHandler().onError(hSocketContext,exc);
    }
}
