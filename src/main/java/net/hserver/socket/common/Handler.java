package net.hserver.socket.common;


public interface Handler {
     void onConnect(HSocketContext hSocketContext);

     void onMessage( HSocketContext hSocketContext,byte[] data);

     void onError( HSocketContext hSocketContext,Throwable throwable);
}
