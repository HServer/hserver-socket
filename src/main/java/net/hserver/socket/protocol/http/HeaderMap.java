package net.hserver.socket.protocol.http;

import java.util.concurrent.ConcurrentHashMap;

public class HeaderMap extends ConcurrentHashMap<String, String> {

        @Override
        public String put(String key, String value) {
            return super.put(key.toLowerCase(), value);
        }

        public String get(String key) {
            return super.get(key.toLowerCase());
        }
    }