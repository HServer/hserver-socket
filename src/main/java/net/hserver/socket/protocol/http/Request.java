package net.hserver.socket.protocol.http;


import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Request {

    private HttpMethod httpMethod;

    private String uri;

    private HeaderMap headers;

    private Map<String, List<String>> bodyParams = new ConcurrentHashMap<>();

    private Map<String, List<String>> urlParams = new ConcurrentHashMap<>();


    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public HeaderMap getHeaders() {
        return headers;
    }

    public void setHeaders(HeaderMap headers) {
        this.headers = headers;
    }

    public Map<String, List<String>> getBodyParams() {
        return bodyParams;
    }

    public void setBodyParams(Map<String, List<String>> bodyParams) {
        this.bodyParams = bodyParams;
    }

    public Map<String, List<String>> getUrlParams() {
        return urlParams;
    }

    public void setUrlParams(Map<String, List<String>> urlParams) {
        this.urlParams = urlParams;
    }
}
