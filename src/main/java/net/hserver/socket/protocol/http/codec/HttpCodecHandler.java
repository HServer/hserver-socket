package net.hserver.socket.protocol.http.codec;

import net.hserver.socket.common.HSocketContext;
import net.hserver.socket.common.Handler;
import net.hserver.socket.protocol.http.Request;

public class HttpCodecHandler implements Handler {

    private Request request = new Request();

    private boolean isHttp(int magic1, int magic2) {
        return
                magic1 == 'G' && magic2 == 'E' || // GET
                        magic1 == 'P' && magic2 == 'O' || // POST
                        magic1 == 'P' && magic2 == 'U' || // PUT
                        magic1 == 'H' && magic2 == 'E' || // HEAD
                        magic1 == 'O' && magic2 == 'P' || // OPTIONS
                        magic1 == 'P' && magic2 == 'A' || // PATCH
                        magic1 == 'D' && magic2 == 'E' || // DELETE
                        magic1 == 'T' && magic2 == 'R' || // TRACE
                        magic1 == 'C' && magic2 == 'O';   // CONNECT
    }


    @Override
    public void onConnect(HSocketContext hSocketContext) {

    }

    @Override
    public void onMessage(HSocketContext hSocketContext, byte[] data) {
        if (data.length > 2) {
            if (isHttp(data[0], data[1])) {
                Request request = new Request();
            }
        }
    }

    @Override
    public void onError(HSocketContext hSocketContext, Throwable throwable) {

    }
}
