package net.hserver.sockt;

import net.hserver.socket.common.HSocketContext;
import net.hserver.socket.common.Handler;
import net.hserver.socket.client.HSocketClient;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.charset.StandardCharsets;

public class Client {

    public static void main(String[] args) throws Exception {
        HSocketClient hSocketClient = new HSocketClient("127.0.0.1", 8888);
        hSocketClient.addHandler(new Handler() {
            @Override
            public void onConnect(HSocketContext channel)  {
                try {
                    channel.write(("本地时间：" + System.currentTimeMillis()).getBytes(StandardCharsets.UTF_8));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onMessage(HSocketContext channel, byte[] data) {
                try {
                    System.out.println("消息：" + new String(data, StandardCharsets.UTF_8));
                    channel.write(("本地时间：" + System.currentTimeMillis()).getBytes(StandardCharsets.UTF_8));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(HSocketContext channel, Throwable throwable) {
                System.out.println("异常："+throwable.getMessage());
            }
        });
        hSocketClient.start();
        Thread.sleep(100000);
    }
}
