package net.hserver.sockt;

import net.hserver.socket.common.HSocketContext;
import net.hserver.socket.common.Handler;
import net.hserver.socket.server.HSocketServer;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.charset.StandardCharsets;

public class Server {

    public static void main(String[] args) {
        new HSocketServer(8888).addHandler(new Handler() {

            @Override
            public void onConnect(HSocketContext channel) {

            }

            @Override
            public void onMessage(HSocketContext channel, byte[] data) {
                System.out.println("消息："+new String(data, StandardCharsets.UTF_8));
                channel.write(("服务时间："+System.currentTimeMillis()).getBytes(StandardCharsets.UTF_8));
            }

            @Override
            public void onError(HSocketContext channel, Throwable throwable) {
                System.out.println("异常："+throwable.getMessage());
            }
        }).start();
    }
}
